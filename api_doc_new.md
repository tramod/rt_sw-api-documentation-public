# New REST API for TraMod
This API design should follow the best practices.
## Models

```
GET     /models	                    # list of models
POST	/models	                    # create new model
DELETE	/models/{model}	            # delete the model
GET     /models/{model}/load	    # load model with model-default matrix to memory
GET     /models/{model}/{matrix_name}/load	    # load matrix to already loaded model
```
##### List of models
```json
[
  {
    "model": "modelName"
  }
]
```
##### Create new model
```json
{
  "model": "modelName"
}
```


## Computation
```
GET     /jobs/{model}                       # list of jobs
POST    /jobs/{model}                       # start computing
GET     /jobs/{model}/{job_id}/status       # job status
GET     /jobs/{model}/{job_id}/result       # job result
DELETE  /jobs/{model}/{job_id}              # kill the job
```
#### List of jobs
Returns the list of jobs for model.
```json
[
  {
    "jobId": "5ffa0a3c65342a594c9de3e29079cd3aa7b03e14",
    "status": "FINISHED",
    "model": "tm_pilsen_new",
    "jobType": "traffic"
  }
]
```
`jobType: traffic, calibration`

#### Start computing (creating new job)
Start to compute the job with given settings. Job type can be `traffic` for static traffic assignment (STA) or `calibration` for Origin-Destination matrix estimation. 
##### Traffic
Assign the ODM to the road network using STA (user equilibrium). An example of job settings is 
```json
{
  "jobType": "traffic",
  "update": [
    {
      "edge_id": 1,
      "capacity": 300,
      "cost": 99999
    }
  ],
  "updateZones": [
    {
        "type": "ADD", "node_id": 1, "source_flow": 156.6, "target_flow": 156.6, "preserve_total_flow": false
    },
    {
        "type": "UPDATE", "zone_id": 2, "source_flow": 156.6, "target_flow": 156.6, "preserve_total_flow": false
    },
    {
        "type": "DELETE", "zone_id": 3, "preserve_total_flow": false
    }
  ],
  "updateTR": [
    {
        "type": "ADD", "node_id": 1, "from_edge_id": 10, "to_edge_id": 11, "cost": -1
    },
    {
        "type": "UPDATE", "node_id": 2, "from_edge_id": 10, "to_edge_id": 15, "cost": 9999
    },
    {
        "type": "DELETE", "node_id": 3, "from_edge_id": 15, "to_edge_id": 16
    }
  ],
  "updateODM": [
    {
      "origin_zone_id": 12, "destination_zone_id": 15, "flow": 126.8
    } 
  ],
  "updateNodes": [
    {
      "node_id": 123
    }
  ],
  "updateEdges": [
    {
      "edge_id": 123, "source_node_id": 456, "target_node_id": 567, "cost": 0.01, "capacity": 1000
    }
  ],
  "cacheName": "cacheName",
  "matrixId": 1,
  "epsilon": 10E-3,
  "threadsCount": 2,
  "maxIter": 10,
  "deterrenceAlpha": 1.0,
  "deterrenceBeta": 1.0,
  "gateway": false,
  "gatewayUrl": "http://example.com/gateway",
  "gatewayTopic": "traffic_model"
}
```

| parameter       | description                                                                   | optional  |
|-----------------|-------------------------------------------------------------------------------|-----------|
| jobType         | type of the job (here `traffic`)                                              | false  |
| cacheName       | if the name of the cache is defined the result will be stored to the database | true  |
| matrixId        | id of the matrix to use                                                       | true  |
| epsilon         | the maximal relative gap                                                      | true  |
| threadsCount    | number of thread for computation                                              | true  |
| maxIter         | maximal number of iteration                                                   | true  |
| deterrenceAlpha | alpha parameter for deterrence function                                       | true  |
| deterrenceBeta  | beta parameter for deterrence function                                        | true  |
| gateway         | If true, the finished result will be sent to gatewayUrl (default: false)      | true  |
| gatewayUrl      | URL for sending the traffic result in body of POST                            | true  |
| gatewayTopic    | see to [gateway](#gateway)                                                    | true  |

###### gateway
If property "gateway" is true, the result will be send to "gatewayUrl" in body of POST request. The body will be:
```json
{
  "topic": "${gatewayTopic}",
  "payload": {}
}
```
The `payload` contains the [traffic result](#traffic-result). This can be used for sending to Apache Kafka.


##### Calibration
Estimate the ODM using the traffic count.
```json
{
  "jobType": "calibration",
  "matrixId": 1,
  "datasetId": 4,
  "measured_flow": [
    {
      "edge_id": 125,
      "flow": 146
    }
   ],
  "epsilon": 10E-3,
  "threadsCount": 2,
  "maxIter": 10,
  "maxIterCal": 10,
  "action": "saveToDB",
  "calMatrixName": "nameOfCalibratedMtx"
}
```
| parameter            | description                                                            | optional  | default                                          |
|----------------------|------------------------------------------------------------------------|-----------|--------------------------------------------------|
| jobType              | type of the job (here `calibration`)                                   | false  |                                                  |
| matrixId             | id of a matrix for calibration                                         | true | not implemented now, default is used in any case |
| datasetId            | ID of a dataset with measured data                                     | false |                                                  |
| measured_flow        | overwrites values in the dataset                                       | true | []                                               |
| sigmaEntropy         | node update parameter in the interval [0, 1]; recommended value is 0.5 | true  | 0.5                                              |
| epsilonEntropy       | ????? (epsilon for XieNie)                                             | true  | 100.0                                            |
| epsilon              | the maximal relative gap                                               | true  | 1E-5                                             |
| threadsCount         | number of threads for computation                                      | true  | 1                                                |
| maxIter              | maximal number of iteration of assignment                              | true  |                                                  |
| maxIterCal           | maximum number of iteration of calibration                             | true  | 10                                               |
| action               | "returnBack", "saveToDB", "nothing"                                    | true | "nothing"                                        | |
| calMatrixDescription | a description of the matrix                                            | true |                                                  |
| calMatrixValidFrom   | start of validity of the matrix                                        | true |                                                  |
| calMatrixValidTo     | end of validity of the matrix                                          | true |                                                  |


| action | description |
|--------|-------------|
| returnBack | return ODM as JSON, !NOT IMPLEMENTED YET! | 
| saveToDB | save ODM to database and return metadata | 
| nothing | return metadata and throw out ODM - DEFAULT |

#### Job status
```json
{
  "status": "ERROR"
}
```
`status: RUNNING, ERROR, FINISHED`

#### Job result
Result of the job. Job type can be `traffic` or `calibration`.
```json
{
  "resultType": "TEMP",
  "jobType": "traffic",
  "result": {}
}
```
`type: TEMP, FINAL`
##### Traffic result
```json
{
  "traffic": [
    {
      "edge_id": 125,
      "traffic": 156.6
    }
  ],
  "tr": [
    {
      "node_id":  1,
      "from_edge": 10,
      "to_edge": 11,
      "traffic": 156.4
    }
  ]
}
```
##### Calibration result
Calibrated ODM. The precision of the solution represents the standard deviation. 
The list of OD pair objects are returned. The `calibrated_odm` is returned only if `action` = `returnBack`.
```json
{
  "standard_deviation": 129.14,
  "standard_deviations": [150.25, 143.47, 129.14],
  "calibrated_odm": [ 
    {
      "source_id": 1,
      "source_node_id": 2,
      "target_id": 3,
      "target_node_id": 4,
      "flow": 1.1
    }
  ],
  "matrixId": 5
}
```


## Cache
```
GET     /caches/{model}                 # list of caches
GET     /caches/{model}/{cache_name}    # get cache
DELETE  /caches/{model}/{cache_name}    # delete cache
```
#### List of caches
```json
[
  {
    "cacheName": "cacheName",
    "timestamp": "2019-01-29 12:20:34"
  }
]
```

#### Cache
```json
{
  "timestamp": "2019-01-29 12:20:34",
  "cacheName": "cacheName",
  "config": {},
  "result": {}
}
```

| parameter | type |
|-----------|------|
|config     | Job configuration for computation|
|result     | Job result|

## Nodes
```
GET     /nodes/{model}              # get all nodes in GeoJSON
POST    /nodes/{model}              # create new node
GET     /nodes/{model}/{node_id}    # get node
PUT     /nodes/{model}/{node_id}    # update node
DELETE  /nodes/{model}/{node_id}    # delete node
GET     /nodes/{model}/info         # get statistics (maximal node ID)
```

#### Node object
```json
{
  "type": "Feature",
  "geometry": {
    "type": "Point",
    "coordinates": [125.6, 10.1]
  },
  "properties": {
    "node_id": 15
  }
}
```
`node_id` is optional

## Edges
```
GET     /edges/{model}              # get all edges in GeoJSON
POST    /edges/{model}              # create new edge
GET     /edges/{model}/{edge_id}    # get edge
PUT     /edges/{model}/{edge_id}    # update edge
DELETE  /edges/{model}/{edge_id}    # delete edge
GET     /edges/{model}/info         # get statistics (maximal edge ID)
```

#### Edge object
```json
{
    "geometry": {
        "type": "LineString", 
        "coordinates": [[0, 0], [125.6, 10.1]]
    },
    "type": "Feature",
    "properties": {
        "edge_id": 15,
        "capacity": 1000.0,
        "source": 4476,
        "target": 4465,
        "cost": 0.000832817,
        "speed":  50
    }
}
```
| property | description | optional |
|----------|-------------|----------|
|edge_id| ID of the edge| true|
|capacity| capacity of the road [veh/h] | false|
|cost| [h]| true/false|
|speed| [km/h]| true/false|
|source| source node id| false|
|target| target node id| false|
`speed` or `cost` must be specified


## Zones
```
GET     /zones/{model}              # get all zones in GeoJSON
POST    /zones/{model}              # create new zone
GET     /zones/{model}/{zone_id}    # get zone
PUT     /zones/{model}/{zone_id}    # update zone
DELETE  /zones/{model}/{zone_id}    # delete zone
```

#### Zone object
```json
{
  "type": "Feature",
  "geometry": {
    "type": "Point",
    "coordinates": [125.6, 10.1]
  },
  "properties": {
    "zone_id": 1,
    "node_id": 25,
    "flow": 123.6
  }
}
```
`zone_id` is optional


## Turn restrictions
```
GET     /trs/{model}                                            # get all turn restrictions
POST    /trs/{model}                                            # crete new TR
GET     /trs/{model}/{node_id}                                  # get turn restrictions by node_id
PUT     /trs/{model}/{node_id}/{from_edge_id}/{to_edge_id}      # update TR for node (intersection)
DELETE  /trs/{model}/{node_id}/{from_edge_id}/{to_edge_id}      # delete TR
```

#### Turn restriction object
```json
{
  "node_id": 1,
  "from_edge_id": 0,
  "to_edge_id": 1,
  "cost": 99.0
 }
```


## ODM
```
GET     /pairs/{model}/{matrix_name}                        # get all OD pairs of specified matrix (get specified ODM)
POST    /pairs/{model}/{matrix_name}                        # create new OD pair
PUT     /pairs/{model}/{matrix_name}/{from_zone}/{to_zone}  # update OD pair
DELETE  /pairs/{model}/{matrix_name}/{from_zone}/{to_zone}  # delete OD pair from specified matrix by zones IDs
```

#### OD pair object
```json
{
  "source_id": 1,
  "source_node_id": 2,
  "target_id": 3,
  "target_node_id": 4,
  "flow": 1.1
}
```

## Dataset
```
GET     /dataset/{model}                    # get all datasets in specified model
POST    /dataset/{model}                    # create new dataset in specified model
GET     /dataset/{model}/{dataset_name}     # get dataset
PUT     /dataset/{model}/{dataset_name}     # update dataset
DELETE  /dataset/{model}/{dataset_name}     # delete dataset
```

#### Dataset object
```json
{
  "dataset_id": 1,
  "name": "datasetName",
  "description": "This is a description of the dataset.",
  "valid_from": "2022-02-22T22:22Z",
  "valid_to": "2024-12-31T23:59Z"
}
```
By adding "flows" property when creating a new dataset (`POST /dataset/{model}`)
it is possible to add flows in the dataset: 
```json
{
  "dataset_id": 1,
  "name": "datasetName",
  "description": "This is a description of the dataset.",
  "valid_from": "2022-02-22T22:22Z",
  "valid_to": "2024-12-31T23:59Z",
  "flows": [
    {
      "edge_id": 2,
      "flow": 22.22
    },
    {
      "edge_id": 3,
      "flow": 234.432
    }
  ]
}
```
| property    | description                                | optional |
|-------------|--------------------------------------------|----------|
| dataset_id  | if not specified, next ID will be assigned | true     |
| name        | name of the dataset                        | false    |
| description |                                            | true     |
| valid_from  | timestamp                                  | true     |
| valid_to    | timestamp                                  | true     |
| flows       | add following flow to the dataset          | true     |


## Matrix
```
GET     /matrix/{model}                             # get all matrices in specified model
POST    /matrix/{model}                             # create new matrix in specified model
GET     /matrix/{model}/{matrix_id}                 # get matrix
PUT     /matrix/{model}/{matrix_id}                 # update matrix
PUT     /matrix/{model}/{matrix_id}/set_default     # sets the matrix as default (and unsets others)
DELETE  /matrix/{model}/{matrix_id}                 # delete matrix
POST    /matrix/{model}/trip_distribution           # create new matrix by calculation of trip distribution
```

#### Matrix object
```json
{
  "matrix_id": 1,
  "dataset_id": 2,
  "name": "matrixName",
  "description": "This is a description of the matrix.",
  "is_default": true,
  "valid_from": "2022-02-22T22:22Z",
  "valid_to": "2024-12-31T23:59Z"
}
```
Only one matrix in the model can be default (`is_default = true`). If the newly created or updated matrix has `is_default` set to `true`, then any previously default matrix would have `is_default` set to `false`. 

#### Trip distribution
Calculate the matrix from incoming and outgoing trips to/from zones. The trip distribution is based on zone distances and combined deterrence function.
```json
{
  "action": "saveToDB",
  "alpha": 1.0,
  "beta": 1.0,
  "tolerance": 1e-6,
  "max_iterations": 1000,
  "matrix_id": 1,
  "matrix_name": "no-name",
  "matrix_description": "This is a description of the matrix.",
  "matrix_is_default": false,
  "matrix_valid_from": "2022-02-22T22:22:22Z",
  "matrix_valid_to": "2024-12-31T23:59:00Z"
}
```
| parameter           | description                                                                                                             | optional  | default       |
|---------------------|-------------------------------------------------------------------------------------------------------------------------|-----------|---------------|
| action              | "returnBack", "saveToDB", "nothing"                                                                                     | true  | "nothing"     |
| alpha               | alpha parameter of deterrence function                                                                                  | either alpha and beta or deterrence_max (and possibly deterrence_wideness) must be specified |               |
| beta                | beta parameter of deterrence function                                                                                   | see alpha |               |
| deterrence_max      | distance for which the function reaches its maximum (argument of the deterrence function maximum)                       | see alpha |               |
| deterrence_wideness | wideness of deterrence function (ratio of the distance between the inflection point arguments and the maximum argument) | true | 1.0           |
| tolerance           | beta parameter for deterrence function                                                                                  | true  | 1E-3          |
| max_iterations      | beta parameter for deterrence function                                                                                  | true  | 1000          |
| matrix_id           | ID of a the matrix                                                                                                      | true  | new unused ID |
| matrix_name         | a name of a the matrix                                                                                                  | true  | null          |
| matrix_description  | a description of the matrix                                                                                             | true  | null          |
| matrix_valid_from   | start of validity of the matrix                                                                                         | true  | null          |
| matrix_valid_to     | end of validity of the matrix                                                                                           | true  | null          |	

| action | description |
|--------|-------------|
| returnBack  | return matrix origin-destination pairs (like GET /pairs/{model}/{matrix_name}) | 
| saveToDB    | save matrix to database and return matrix (like GET /matrix/{model}/{matrix_name})| 
| nothing     | calculate and throw out the matrix - DEFAULT |

## Flow
```
GET     /flow/{model}/{dataset_id}            # get all flows in specified dataset
POST    /flow/{model}/{dataset_id}            # create new flow / multiple flows in specified dataset
PUT     /flow/{model}/{dataset_id}            # update flow in specified dataset
DELETE  /flow/{model}/{dataset_id}            # delete all flows in specified dataset
GET     /flow/{model}/{dataset_id}/{edge_id}  # get flow
PUT     /flow/{model}/{dataset_id}/{edge_id}  # update flow
DELETE  /flow/{model}/{dataset_id}/{edge_id}  # delete flow
```

#### Flow object
```json
{
  "dataset_id": 1,
  "edge_id": 2,
  "flow": 22.4
}
```

When creating multiple flows `POST /flow/{model}/{dataset_id}` the flows are expected as array:
```json
{
  "flows": [
    {
      "edge_id": 2,
      "flow": 22.4
    },
    {
      "edge_id": 3,
      "flow": 2.0
    }
  ]
}
```

It means you can add flows one by one or as a batch through `POST /flow/{model}/{dataset_id}`.

## Info
```
POST	/number_jobs				# number of running jobs and other useful information
GET     /health                     # health check
```

### Number of running jobs
Number of current running jobs.
```json
{
  "numberOfRunningJobs": 0
}
```
### Health check
Check the database connection and computation.
```json
{
  "database": true,
  "computation": true,
  "isOK": true
}
```

## The Shortest paths
```
GET	/paths/{model}				# shortest paths
```

#### paths
Parameters:
* model (in path) - name of the model
* job_id (request param, optional) - scenario that provides the costs for searching
* cache_name (request param, optional) - scenario from cache that provides the costs for searching
* from_node (request param, required) - start node ID 
* to_node (request param, optional) - end node ID; if not specified, the paths to all nodes are computed

if job_id and cache_name are not specified, the free flow costs are used.

e.g.,
  `http://localhost:8080/paths/tm_test?from_node=1&to_node=5`

#### result
```json
[
  {
    "from_node": 1,
    "to_node": 5,
    "edge_ids": [2, 8]
  }
]
```
* edge_ids - path (list of edge_id)