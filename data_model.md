# TraMod data structure
TraMod data structure consists of:
- Traffic model fundamental tables:
  - Edge 
  - Node
  - Zone
  - ODM (OD Matrix)
  - Turn_restriction
  - Dataset
  - Measured_flow
  - Matrix

#### Units
| Name               | Unit                 | Shortcut |
|--------------------|----------------------|----------|
| distance           | kilometers           | km       |
| time               | hours                | h        |
| speed              | kilometeres per hour | km/h     |
| traffic/flow/trips | vehicles per hour    | veh/h    |

### Edge
| Name                                  | Data type                   | Description                                                                                                                                                                                                                                                               | References                  |
|---------------------------------------|-----------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------|
| edge_id *<sup>required</sup>          | serial                      | Unique edge number / identifier                                                                                                                                                                                                                                           | PK                          |
| source *<sup>required</sup>           | int                         | Initial node of the edge                                                                                                                                                                                                                                                  | FK references node(node_id) |
| target *<sup>required</sup>           | int                         | Final node of the edge                                                                                                                                                                                                                                                    | FK references node(node_id) |
| capacity *<sup>required</sup>         | real                        | <p>Maximum number of vehicles which can pass the edge per hour [vehicles/hour]</p> <p>Usually the capacity is set up to 1800 vehicles per hour per one lane</p>                                                                                                           |                             |
| isvalid *<sup>required</sup>          | boolean                     | <p><i>True</i>: edge is included to the model calculation</p> <p><i>False</i>: edge is not included in the model calculation <p>Can be used for planned roads, which are ready in the model, but not applicable to be modelled yet</p>                                    |                             |
| turn_restriction *<sup>required</sup> | text                        | <p>Defines turn restrictions</p> <p><i>default</i>: ""</p> <p>The attribute must exist for backward compatibility; leave it blank, use turn_restriction table instead</p>                                                                                                 |                             |
| speed *<sup>required</sup>            | real                        | Maximum speed [km/h]                                                                                                                                                                                                                                                      |                             |
| road_type                             | int                         | Number of road type. Based on <a href="https://github.com/FoxtrotSystems/osm2po-foxtrot/blob/master/osm2po.config">clazz</a> identifier from osm2po                                                                                                                       |                             |
| isvirtual                             | boolean                     | <p><i>True</i>: edge used in the model, but not visible in the application</p> <p><i>False</i>: edge used in the model and  also visible in the application</p> <p> Attribute can be set up True for connectors: edges connecting Zones to nearest road network Nodes</p> |                             |
| geometry *<sup>required</sup>         | geometry (LineString, 4326) | LineString geometry in the WGS84 coordinate system (EPSG:4326)                                                                                                                                                                                                            |                             |

### Node
| Name                          | Data type              | Description                                               | References |
|-------------------------------|------------------------|-----------------------------------------------------------|------------|
| node_id *<sup>required</sup>  | int                    | Unique Identifier of each road network node               | PK         |
| geometry *<sup>required</sup> | geometry (Point, 4326) | Point geometry in the WGS84 coordinate system (EPSG:4326) |            |

### Zone
| Name                          | Data type                                        | Description                                                     | References              |
|-------------------------------|--------------------------------------------------|-----------------------------------------------------------------|-------------------------|
| zone_id *<sup>required</sup>  | int                                              | Unique identifier of each Zone generating traffic               | PK                      |
| node_id *<sup>required</sup>  | int                                              | Identifier of corresponding Node (connected by connector Edges) | FK **referencing** Node |
| trips *<sup>required</sup>    | real                                             | Sum of values from the OD matrix for the given zone             |                         |
| incoming_trips                | int                                              | Number of trips that ends in zone                               |                         |
| outgoing_trips                | int                                              | Number of trips that originates in zone                         |                         |
| geometry *<sup>required</sup> | geometry (Point, 4326) <b>optional: Polygon?</b> | Point geometry in the WGS84 coordinate system (EPSG:4326)       |                         |

### ODM
| Name                             | Data type | Description                                                     | References                      |
|----------------------------------|-----------|-----------------------------------------------------------------|---------------------------------|
| source *<sup>required</sup>      | int       | Incoming zone number                                            | PK, FK references zone(zone_id) |
| source_node *<sup>required</sup> | int       | Identifier of a network Node corresponding to the incoming zone | FK **referencing** Node         |
| target *<sup>required</sup>      | int       | Outgoing zone number                                            | PK, FK references zone(zone_id) |
| target_node *<sup>required</sup> | int       | Identifier of a network Node corresponding to the outgoing zone |                                 |
| flow *<sup>required</sup>        | real      | Value from the OD matrix                                        |                                 |
| matrix_id *<sup>required</sup>   | int       | Link to the ODM                                                 | PK, FK **referencing** Matrix   |

### Turn_restriction
| Name                              | Data type | Description                                                                                                | References                      |
|-----------------------------------|-----------|------------------------------------------------------------------------------------------------------------|---------------------------------|
| node_id *<sup>required</sup>      | int       | Unique node number                                                                                         | PK, FK references node(node_id) |
| from_edge_id *<sup>required</sup> | int       | Incoming edge number                                                                                       | PK, FK references edge(edge_id) |
| to_edge_id *<sup>required</sup>   | int       | Outgoing edge number                                                                                       | PK, FK references edge(edge_id) |
| cost *<sup>required</sup>         | real      | <p>Cost of crossing the intersection (in a given direction) in hours</p> <p><i>-1</i>: prohibited turn</p> |                                 |

### Dataset
| Name                            | Data type | Description         | References |
|---------------------------------|-----------|---------------------|------------|
| dataset_id *<sup>required</sup> | int       | Unique dataset id   | PK         |
| name *<sup>required</sup>       | string    | name of the dataset |            |
| description                     | string    |                     |            |
| valid_from                      | timestamp | validity            |            |
| valid_to                        | timestamp | validity            |            |

### Measured flow
| Name                            | Data type | Description                          | References                     |
|---------------------------------|-----------|--------------------------------------|--------------------------------|
| dataset_id *<sup>required</sup> | int       | Unique dataset id                    | PK, FK **referencing** Dataset |
| edge_id *<sup>required</sup>    | int       |                                      | PK, FK **referencing** Edge    | 
| flow                            | real      | Measured traffic on edge in veh/hour |                                |

### Matrix
List of Origin-Destination matrices

| Name                            | Data type | Description                                                 | References                     |
|---------------------------------|-----------|-------------------------------------------------------------|--------------------------------|
| matrix_id *<sup>required</sup>  | int       | Unique matrix id                                            | PK                             |
| name *<sup>required</sup>       | string    | name of the dataset                                         |                                |
| description                     | string    |                                                             |                                |
| is_default *<sup>required</sup> | boolean   | Define if this ODM is default                               |                                |
| valid_from                      | timestamp | validity                                                    |                                |
| valid_to                        | timestamp | validity                                                    |                                |
| dataset_id                      | int       | Link to dataset. Means "this ODM is calibrated by dataset". | FK, FK **referencing** Dataset |


